package com.nearbuy.qa.seleniumbase;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.nearbuy.qa.util.PropertiesUtil;

public class SeleniumBase {

	private static ThreadLocal<WebDriver> WEB_DRIVERS_PROVIDER = new ThreadLocal<WebDriver>();

	protected static WebDriver getWebDriver() {
		return WEB_DRIVERS_PROVIDER.get();
	}

	private static void setWebDriver(WebDriver driver) {
		WEB_DRIVERS_PROVIDER.set(driver);
	}


	protected SeleniumBase(){
	}

	protected SeleniumBase(String browser, String os) throws IOException{

		if(getWebDriver() == null)
			setWebDriver(invokeBrowser(browser, os));
	}


	/**********************/	

	public static synchronized void initializeDriver(String browser, String os) throws IOException{
		if(getWebDriver() == null)
			setWebDriver(invokeBrowser(browser, os));
	}


	protected static void closeDriver(){
		try{
			getWebDriver().close();
		}catch(Exception ex){
			System.err.println(ex);
		}
	}

	public static void quitDriver(){
		try{
			closeDriver();
			getWebDriver().quit();
			WEB_DRIVERS_PROVIDER.remove();
		}catch(Exception ex){
			System.err.println(ex);
		}
	}



	private static WebDriver invokeBrowser (String browser, String os) throws IOException {

		WebDriver driver = null ;
		boolean append	=	false;
		String xPort = System.getProperty("Importal.xvfb.id",":1");
		ChromeDriverService chromeDriverService = null;
		System.out.println("Value of port" +xPort);
		File f	=	new File(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources");
		if(!f.exists())
			append	=	true;

		System.out.println("browser = "+ browser);
		if(browser.equalsIgnoreCase("firefox"))
		{
			DesiredCapabilities dc=new DesiredCapabilities();
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);

			if (os.equalsIgnoreCase("linux")){
				FirefoxBinary fb = new FirefoxBinary();
				fb.setEnvironmentProperty("DISPLAY", xPort);

				driver =new FirefoxDriver(fb, null, dc);
			}else {

				driver =new FirefoxDriver(dc);
			}
		}
		else if(browser.equalsIgnoreCase("chrome"))
		{
			if(os.equalsIgnoreCase("win"))
			{
				if(!append)
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("WindowsChromeDriver"));
				else
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("ProjectName")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("WindowsChromeDriver"));
			}
			//download chromedriver for mac
			else if(os.equalsIgnoreCase("mac")) {
				//System.out.println("Inside Chrome");
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("MacChromeDriver"));

			}else if(os.equalsIgnoreCase("linux")) {
				System.out.println("Inside Chrome for linux");
				chromeDriverService = new ChromeDriverService.Builder().usingDriverExecutable(new File(PropertiesUtil.getConstantProperty("LinuxChromeDriver"))).usingAnyFreePort().withEnvironment(ImmutableMap.of("DISPLAY", xPort)).build();
				chromeDriverService.start();
				System.out.println("Chrome server started");
			}

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--test-type");
			if (chromeDriverService != null)
				driver = new ChromeDriver(chromeDriverService, options);
			else 
				driver = new ChromeDriver(options);
		}
		else if(browser.equalsIgnoreCase("ie"))
		{
			if(!append)
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("IEDriver"));
			else
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("ProjectName")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("IEDriver"));	
			driver = new InternetExplorerDriver();
		}
		else if(browser.equalsIgnoreCase("safari"))
			driver	=	new SafariDriver();

		driver.manage().window().maximize();

		return driver;

		//		if(browser.equalsIgnoreCase("chrome")&&os.equalsIgnoreCase("mac"))
		//		BrowserUtil.setWindowToFullSize("testwindow",true);

	}

	protected void takeScreenShot(ITestResult result ){

		String failureImageFileName =  result.getName() + new SimpleDateFormat("MM-dd-yyyy_HH-ss").format(new GregorianCalendar().getTime())+ ".png";

		try {
			File scrFile = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("ScreenshotPath")+System.getProperty("file.separator")+failureImageFileName)); 

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Scrolling the page for getting element in focus
	 * @param ele 
	 */

	protected void scrollElementIntoView(WebElement ele) {
		((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(false);", ele);
		((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,300);", "");
		sleepThread(1000);
	}

	protected void scrollElementToTop(WebElement ele) {
		((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(false);", ele);
	}

	protected String acceptAlertAndReturnMessage(){
		Alert alert = getWebDriver().switchTo().alert();
		String message = alert.getText();  
		alert.accept();
		return message;
	}

	protected void hoverOnElement(WebElement ele){

		Actions action = new Actions(getWebDriver());
		action.moveToElement(ele).build().perform();
	}

	//	public static void setWindowToFullSize(String windowName,boolean closeCurrentWindow)
	//	{
	//		JavascriptExecutor js = (JavascriptExecutor)driver;
	//		js.executeScript("window.open('','"+windowName+"','width=400,height=200')");
	//		if(closeCurrentWindow)
	//		driver.close();
	//		driver.switchTo().window(windowName);
	//		js.executeScript("window.moveTo(0,0);");
	//		js.executeScript("window.resizeTo(1280,800);");	
	//	}

	/**********************/	
	protected void fn_SelectRightClickOption(WebElement we, int optionIndex) throws InterruptedException{
		Actions actObj=new Actions(getWebDriver());
		actObj.contextClick(we).build().perform();
		Thread.sleep(500);
		for(int i=1; i <= optionIndex; i++){
			actObj.sendKeys(Keys.ARROW_DOWN);
		}
		actObj.sendKeys(Keys.ENTER);
	}

	protected void fn_OpenURL(String url){
		System.out.println(getWebDriver());
		getWebDriver().get(url);
	}

	protected void fn_RefreshPage(){
		getWebDriver().navigate().refresh();
	}

	protected void fn_Click(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			weObj.click();
	}

	protected void fn_MouseOver(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			new Actions(getWebDriver()).moveToElement(weObj).build().perform();
	}

	protected void fn_MouseClick(WebElement weObj){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			new Actions(getWebDriver()).click(weObj).build().perform();
	}

	protected void fn_JavaScriptClick(WebElement we){
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("arguments[0].click();", we);
	}

	protected void fn_WindowScroll(){
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("window.scrollTo(0,150);");
	}

	protected void fn_WindowScrollToWebElement(WebElement we){
		Point p= we.getLocation();
		JavascriptExecutor executor = (JavascriptExecutor)getWebDriver();
		executor.executeScript("window.scrollTo("+p.getX()+","+p.getY()+");");
	}

	protected void fn_InputStatic(WebElement weObj, String valToInput){
		boolean visiblestatus=fn_IsVisiible(weObj);
		boolean enablestatus=fn_IsEnable(weObj);
		if(enablestatus==true && visiblestatus==true)
			weObj.sendKeys(valToInput);
	}

	protected boolean fn_IsVisiible(WebElement eleObj){
		int height = eleObj.getSize().getHeight();
		if(height>0){
			return true; 
		}else{
			return false;
		}
	}

	protected boolean fn_IsEnable(WebElement eleObj){
		boolean status=eleObj.isEnabled();
		return status;
	}  

	protected void fn_ExplicitWait_Visibility(WebElement we, int TimeOut){
		WebDriverWait WWT=new WebDriverWait(getWebDriver(), TimeOut);
		WWT.until(ExpectedConditions.visibilityOf(we));
	}

	protected void fn_ExplicitWait_ClickAble(WebElement we, int TimeOut){
		WebDriverWait WWT=new WebDriverWait(getWebDriver(), TimeOut);
		WWT.until(ExpectedConditions.elementToBeClickable(we));
	}  

	protected void fn_ValidateText_Static(WebElement we, String ExpectedValue){
		String ActualText= we.getText();
		if(ExpectedValue.trim().equalsIgnoreCase(ActualText)){
			String[] res_arr={};

			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	} 

	protected void fn_ValidateTextContains_Static(WebElement we, String ExpectedValue){
		boolean ActualStatus= we.getText().contains(ExpectedValue);
		if(ActualStatus==true){
			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	}  

	protected void fn_ValidateAttribute_Static(WebElement we, String ExpectedValue){
		boolean ActualStatus= we.getText().contains(ExpectedValue);
		if(ActualStatus==true){
			System.out.println("Passed");
		}else{
			System.out.println("Failed");
		}
	}

	protected void switchToPopup(String title){
		Set<String> gmailWindows = getWebDriver().getWindowHandles();
		for(String gmailWindow : gmailWindows){

			getWebDriver().switchTo().window(gmailWindow);
			if(getWebDriver().getTitle().contains(title))
				break;
		}
	}

	protected void switchToParentWindow(Set<String> windows){

		for(String window : windows){
			getWebDriver().switchTo().window(window);
			//System.out.println("Window name in external ::" +window + "-" + driver.getTitle() );
			break;
		}
	}


	protected WebElement fluentWait(final By locator) 
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver()).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement webElement = wait.until(new Function<WebDriver, WebElement>()
		{
			public WebElement apply(WebDriver driver) 
			{
				return driver.findElement(locator);

			}});
		return webElement;
	};

	protected WebElement fluentWaitForElementTextLength(final WebElement webelement,int totalTime, int pollingTime) 
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver()).withTimeout(totalTime, TimeUnit.SECONDS).pollingEvery(pollingTime, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement webElement= wait.until(new Function<WebDriver, WebElement>()
		{
			public WebElement apply(WebDriver driver) 
			{
				if(webelement.getText().length()>0)	
					return webelement; 
				//return driver.findElement(By.id("invalid"));
				return webelement;


			}});
		return webElement;
	};


	protected void sleepThread(long milliseconds)
	{
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void explicitWaitByVisibilityOfElement( int seconds,WebElement el){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.visibilityOf(el));
		//return wait;
	}

	protected void explicitWaitByPresenceOfElement( int seconds,By by){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		//return wait;
	}


	protected WebDriverWait explicitWaitByElementToBeClickable(int seconds, WebElement elt){
		WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
		wait.until(ExpectedConditions.elementToBeClickable(elt));
		return wait;
	}


	protected WebElement fluentWait(final WebElement locator,
			final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver())
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(500, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return locator;
			}
		});
	}

	public String getCookievalue(String cookieName){
		Cookie cookie = getWebDriver().manage().getCookieNamed(cookieName);  
		String cookieVal = cookie.getValue();
		return cookieVal;
	}
}


