package com.nearbuy.qa.apibase;


import org.testng.ITest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import com.nearbuy.qa.util.PropertiesUtil;

public class APITestBase implements ITest{

	protected String env = null;
	protected String verticalForAps = null;
	protected String emailForReporting = null;
	public static String groups = null;
//	protected String operatingSystem = null;
//	protected String browser = null;
	
	
    String testName = null;

	@BeforeSuite(alwaysRun = true)
	public void setUp() throws Exception {
		//uncommented for OMS
		//apiHelper = new ApiHelper();
		System.out.println("Preparing to Load Constant file..");
		PropertiesUtil.loadConstantFile("Constant.cfg");

	}

	@BeforeTest(alwaysRun = true)
	@Parameters({"environment"})
	public void setUpEnvironment(@Optional("Prod")String environment) throws Exception {
		env = environment;
		String fileName = environment+".cfg";
		System.out.println("Preparing to Load " + fileName + " file..");

		PropertiesUtil.loadEnvConfigFile(fileName);
	}

	@BeforeClass
	@Parameters({"browser","os","vertical","email","group"})
	public void beforeEveryClass(@Optional("chrome")String br,@Optional("mac")String os,@Optional("Local- F&B")String vertical,@Optional("test@nearbuy.com")String email,@Optional("All")String group) throws Exception{
//		operatingSystem = os;
//		browser = br;

//		System.out.println("OS name in beforeEveryCLass in TestBase ::" + os);
//		System.out.println("Browser in beforeEveryCLass in TestBase ::" +br);

		verticalForAps = vertical;
		emailForReporting = email;
		groups = group;
		
		// call to initialize webdriver
//		 SeleniumBase.initializeDriver(br,os);
	}

	
	@BeforeMethod
	public void beforeEveryMethod(Object testdata[]){
		this.testName = (String) testdata[0];
	}

	@AfterClass
	public void afterEveryClass() {
		// clear cache prepared from test data sheet
		//DataProviderUtil.clearTestData();
	}

	@AfterTest
	public void afterEachTestCycle() {
		// terminate web driver if not terminated
//		 SeleniumBase.quitDriver();
	}

	@AfterSuite(alwaysRun = true)
	public void afterSuite() {
//		SeleniumBase.quitDriver();
		//File tempFile=null;
		try {
			

			/*
			tempFile = ReportUtil.extractResultTable(env);
			String testReportCompleteName = TestListener.testReportCompleteName;
			File f = new File(testReportCompleteName);
			
			if(f.exists()){
				EmailUtil.sendEmail(testReportCompleteName,
						emailForReporting+ ","+ PropertiesUtil.getConstantProperty("EmailReciever"),
						PropertiesUtil.getConstantProperty("EmailSender"),
						PropertiesUtil.getConstantProperty("EmailPassword"),
						PropertiesUtil.getConstantProperty("EmailReportSubject"),
						PropertiesUtil.getConstantProperty("EmailReportBody"),tempFile);
			}else{
				System.out.println("REPORT FILE NOT CREATED");
			}
			tempFile.delete();*/
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//tempFile.delete();
		}

	}

	@Override
	public String getTestName() {
		
		return this.testName;
	}


}
