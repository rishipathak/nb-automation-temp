package com.nearbuy.qa.Listener;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.nearbuy.qa.util.ExtentManager;
import com.nearbuy.qa.util.PropertiesUtil;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Test;

public class TestListener implements ITestListener {

	// public static String screen = null;
	private static String exception ;
	private ExtentReports extentReport = null;
	//	private static ExtentTest test = null;

	public static String testReportCompleteName = null;
	public static Test testName = null;

	private static ThreadLocal<ExtentTest> extentTestThreadLocal = new ThreadLocal<ExtentTest>();

	private String testNameFromXml = null;
	
	public void onStart(ITestContext context) {

		File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + PropertiesUtil.getConstantProperty("ReportPath"));

		if (!file.exists()){
			file.mkdir();
		}
		
		DateFormat df = new SimpleDateFormat("dd_MM_yy_HHmmss");
		Date dateobj = new Date();
		String reportName ="Report_" + df.format(dateobj) ;

		String currentReport = reportName + ".html";
		testReportCompleteName = System.getProperty("user.dir") + System.getProperty("file.separator") + PropertiesUtil.getConstantProperty("ReportPath") +  System.getProperty("file.separator") + currentReport;

		System.out.println("testReportCompleteName="+ testReportCompleteName);

		extentReport = ExtentManager.getInstance(testReportCompleteName);

		extentReport.config().reportName("Report :: ");
		extentReport.config().reportHeadline(PropertiesUtil.getConstantProperty("ReportHeadline"));
		extentTestThreadLocal.set(extentReport.startTest("PreCondition ", "Test Run on " + testNameFromXml));
		
		testNameFromXml = context.getName();
		
		System.out.println("class Name -"+this.getClass().getName());
		testNameFromXml = context.getName();
		extentTestThreadLocal.set(extentReport.startTest("PreCondition ", "Test Run on " + testNameFromXml));
		testName = extentTestThreadLocal.get().getTest();
	}

	public void onTestStart(ITestResult result) {
		System.out.println("Started Test: " + result.getName());
		//		test = extentReport.startTest(result.getName(), "Test Run on " + testNameFromXml);
		if (testName == extentTestThreadLocal.get().getTest()){
			extentReport.endTest(extentTestThreadLocal.get());
		}
		extentTestThreadLocal.set(extentReport.startTest(result.getName(), "Test Run on " + testNameFromXml));
		extentTestThreadLocal.get();
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("Finished Test: " + result.getName() + " :PASSED");
		extentTestThreadLocal.get().log(LogStatus.PASS, result.getName() + " Passed");
		extentReport.endTest(extentTestThreadLocal.get());
	}

	public void onTestFailure(ITestResult result) {
		System.out.println("Finished Test: " + result.getName() + " :FAILED");
		if (exception != "")
			extentTestThreadLocal.get().log(LogStatus.FAIL, result.getName() + "<pre><br>" + exception + "</br></pre>" + "Failed");
		else
			extentTestThreadLocal.get().log(LogStatus.FAIL, result.getName() + " :: Failed");
		// test.addScreenCapture(screen);
		extentReport.endTest(extentTestThreadLocal.get());
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("Finished Test: " + result.getName() + " :SKIPPED");
		extentTestThreadLocal.get().log(LogStatus.SKIP, result.getName() + " Skipped");
		extentReport.endTest(extentTestThreadLocal.get());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("Finished Test: " + result.getName() + " :FAILED BUT WITHIN SUCCESS PERCENTAGE");
		extentTestThreadLocal.get().log(LogStatus.WARNING, result.getName() + " Failed");
		extentReport.endTest(extentTestThreadLocal.get());
	}

	public void onFinish(ITestContext context) {
		
		if (extentReport != null){
			extentReport.flush();
		}
	}

	public static String setException(String message, StackTraceElement[] stackTrace) {

		for(StackTraceElement stackTraceElement : stackTrace) {                         
			message = message + System.lineSeparator() + stackTraceElement;
		}
		exception = message;
		return exception;
	}

	public static void clearAllException() {

		exception = "";
	}
	
	public static void setMessage(String message) {

		exception = message;
	}
	
	public static void setLog(LogStatus type, String logMessage){
		extentTestThreadLocal.get().log(type, logMessage);
	}

	public static void setLogsForTestData(String scenario, Map<String, String> inputMap, String expected ){
		try{
			inputMap.remove("testdata");
		}catch(Exception ex){

		}
		//setLog(LogStatus.INFO, "TEST SCENARIO - " + scenario);
		setLog(LogStatus.INFO, "TEST DATA - " + inputMap);
		//setLog(LogStatus.INFO, "EXPECTATION - " + expected);
	}

}
