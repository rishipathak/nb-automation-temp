package com.nearbuy.qa.api.test;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.nearbuy.qa.Listener.TestListener;
import com.nearbuy.qa.apibase.APITestBase;
import com.nearbuy.qa.util.APS_OMS_MappingUtil;
import com.nearbuy.qa.util.LoggerUtil;
import com.nearbuy.qa.util.LoggerUtil.LogLevel;
import com.nearbuy.qa.util.PropertiesUtil;


@Listeners(TestListener.class)
public class ApsOmsMapping  extends APITestBase
{

	APS_OMS_MappingUtil APS_OMS;
	OrderLine_Reporting olr;
	public int ilimit_OMSresult;

	@BeforeClass
	public void initializeClassAPSTest() throws NumberFormatException, Exception
	{
		// 1st class to start
		APS_OMS = new APS_OMS_MappingUtil();
		
		ilimit_OMSresult = 100;
		
		APS_OMS.createDBConnections();

	}
	@AfterClass
	public void revertClassConfiguration() throws InterruptedException{

		Thread.sleep(480000);

		//close db and ssh connections
		APS_OMS.closeDbConnections();

	}


	@Test(dataProvider = "trigger_sale",priority=1)
	public void testSaleTriggerData(String testName)  
	{
		String RunID = PropertiesUtil.getConstantProperty("OMS_APS_RunID");
		olr = new OrderLine_Reporting(RunID);
		olr.Status = "Completed";
		olr.currentOrderLineId="";
		olr.errorList.clear();
		long start_millis = System.currentTimeMillis();
		
		//boolean testStatus = true;
		
		// fetching payment term id from lookup collecion
		String sPaymentTermId_sale = APS_OMS.getPaymetid("sale",olr);
	//	String sPaymentTermId_sale = "100026,100002";
		
		
		//fetch postgree data for oms with trigger as sale

		String screatedFrom = PropertiesUtil.getConstantProperty("OMS_createdAtFrom"); //"yyyy-MM-dd HH:mm:ss"
		String screatedFrom_EPOCHTime =APS_OMS.convertToEpoch(screatedFrom,olr);


		String screatedTill = PropertiesUtil.getConstantProperty("OMS_createdAtTill"); //"yyyy-MM-dd HH:mm:ss"
		String screatedTill_EPOCHTime  = APS_OMS.convertToEpoch(screatedTill,olr);

	// QA Sale PaymentTerms	
	//	String sSalePaymentTerm = "'{\"_id\":null,\"paymentTerm\":\"Travel - Payment on Confirmation (weekly)\",\"paymentTermAttribute\":[{\"attribute\":\"trigger\",\"value\":\"sale\"},{\"attribute\":\"freq\",\"value\":\"weekly\"},{\"attribute\":\"breakage\",\"value\":\"merchant\"},{\"attribute\":\"payment\",\"value\":\"auto\"}]}'"; 

	// Prod Sale Payment Terms
		String sSalePaymentTerm = "'{\"_id\":null,\"paymentTerm\":\"Travel - Payment on Confirmation (weekly)\","
				+ "\"paymentTermAttribute\":[{\"attribute\":\"trigger\",\"value\":\"sale\"},{\"attribute\":\"freq\",\"value\":\"weekly\"}"
						+ ",{\"attribute\":\"breakage\",\"value\":\"merchant\"},{\"attribute\":\"payment\",\"value\":\"auto\"}]}'";
	
		String sPostgreQuery_OMS = "select __columnNames__ from oms_data.orderline where ispaid=true ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "and (createdat between "+screatedFrom_EPOCHTime+" and "+screatedTill_EPOCHTime +") ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "and (paymenttermid in ("+sPaymentTermId_sale+") ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "or paymentTerms = " + sSalePaymentTerm +" ) ";

		String sTotalCountQuery = sPostgreQuery_OMS.replace("__columnNames__", "count(1) as TotalRowCount");
		Statement stmt =null;
		ResultSet rs=null;

		int iTotalCount=0;
		try 
		{
			stmt = APS_OMS.postgre_OMS.connection.createStatement();

			rs= stmt.executeQuery(sTotalCountQuery);
			rs.next();
			iTotalCount = rs.getInt("TotalRowCount");
		} 
		catch (SQLException e) {
			String sError = "Not able to execute OMS query : "+sTotalCountQuery;
			LoggerUtil.setlog(LogLevel.ERROR,sError );
			e.printStackTrace();
			olr.errorList.add(sError);
			olr.Status = "FAILED";
			olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
			Assert.assertTrue(false,sError);
		}

		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Total records in OMS to verify -:"+iTotalCount);
		olr.iTotalCount = iTotalCount;
		
		if(iTotalCount==0)
		{
			LoggerUtil.setlog(LogLevel.ERROR, "No more records to verify");
			olr.errorList.add("No more records to verify");
			//olr.Status = "FAILED";
			//olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);

		}
		else
		{
			int totalCounter = (iTotalCount/ilimit_OMSresult) +1;
			for(int offsetCounter=0;offsetCounter<totalCounter;offsetCounter++)
			{
				String sOffset = Integer.toString(ilimit_OMSresult*offsetCounter);
				String sQuery = sPostgreQuery_OMS.replace("__columnNames__", "cda, dealId, merchantId, merchantName, partnerNumber,offerid, orderid, orderlineid, unitprice,marginpercentage, paymentTerms,paymenttermid, voucherid, unitPrice,createdAt");
				sQuery = sQuery +  "order by orderlineid OFFSET "+ sOffset +" LIMIT "+ Integer.toString(ilimit_OMSresult);
				System.out.println("Sale Query ::" +sQuery );
				try 
				{
					stmt = APS_OMS.postgre_OMS.connection.createStatement();
					rs = stmt.executeQuery(sQuery);
				} 
				catch (SQLException e)
				{
					String sError = "Not able to execute OMS query : "+sQuery;
					LoggerUtil.setlog(LogLevel.ERROR,sError );
					e.printStackTrace();
					olr.errorList.add(sError);
					olr.Status = "FAILED";
					olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
					Assert.assertTrue(false,sError);
				}
				//compare this result set with APS data
				try{
				APS_OMS.validate_OMS_APS(rs,olr);}
				catch(Exception e)
				{
					e.printStackTrace();
					olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
					long end_millis = System.currentTimeMillis();
					
					
					LoggerUtil.setlog(LogLevel.ONLYLOGS, "Test Finish Time -:"+end_millis);
					LoggerUtil.setlog(LogLevel.ONLYLOGS, "Time Taken -:"+(end_millis-start_millis));
					break;
				}
			}
		}
		
		long end_millis = System.currentTimeMillis();
		
		
		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Test Finish Time -:"+end_millis);
		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Time Taken -:"+(end_millis-start_millis));
		olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
		
	}
	@DataProvider(name = "trigger_sale")
	public Object[][] triggerSale() throws Exception {

		String [][] ret = { {"APS_OMS_TriggerSale_Mapping"} };
		return ret;
	}

	@Test(dataProvider = "trigger_redeem",priority=2)
	public void testRedeemTriggerData(String testName)  
	{
		String RunID = PropertiesUtil.getConstantProperty("OMS_APS_RunID");
		olr = new OrderLine_Reporting(RunID);
		olr.Status = "Started";
		olr.currentOrderLineId="";
		olr.errorList.clear();
		long start_millis = System.currentTimeMillis();
		
		
		// fetching payment term id from lookup collecion
		
		// QA Payment Term Ids
		String sPaymentTermId_sale = APS_OMS.getPaymetid("redeem",olr);
		
//		// Prod Payment Term Ids
//		String sPaymentTermId_sale = "100019,100005";

		//fetch postgree data for oms with trigger as sale

		String sredemptionFrom = PropertiesUtil.getConstantProperty("OMS_redemptionFrom"); //"yyyy-MM-dd HH:mm:ss"
		String sredemptionFrom_EPOCHTime =APS_OMS.convertToEpoch(sredemptionFrom,olr);



		String sredemptionTill = PropertiesUtil.getConstantProperty("OMS_redemptionTill"); //"yyyy-MM-dd HH:mm:ss"
		String sredemptionTill_EPOCHTime  = APS_OMS.convertToEpoch(sredemptionTill,olr);

		// QA
		// String sRedeemPaymentTerm = "' {\"_id\":\"2\",\"paymentTerm\":\"Payment on Redemption + 100% Breakage\",\"paymentTermAttribute\":[{\"attribute\":\"trigger\",\"value\":\"redeem\"},{\"attribute\":\"freq\",\"value\":\"weekly\"},{\"attribute\":\"breakage\",\"value\":\"merchant\"},{\"attribute\":\"payment\",\"value\":\"auto\"},{\"attribute\":\"cycle\",\"value\":\"0\"}]}'"; 

		// Prod
		String sRedeemPaymentTerm = "'{\"_id\":\"2\",\"paymentTerm\":\"Payment on Redemption + 100% Breakage\","
				+ "\"paymentTermAttribute\":[{\"attribute\":\"trigger\",\"value\":\"redeem\"},{\"attribute\":\"freq\",\"value\":\"weekly\"},"
						+ "{\"attribute\":\"breakage\",\"value\":\"merchant\"},"
								+ "{\"attribute\":\"payment\",\"value\":\"auto\"},{\"attribute\":\"cycle\",\"value\":\"0\"}]}'";
		
		String sPostgreQuery_OMS = "select __columnNames__ from oms_data.orderline where ispaid=true and voucherstatus='Redeemed' ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "and (redemptiondate between "+sredemptionFrom_EPOCHTime+" and "+sredemptionTill_EPOCHTime +") ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "and (paymenttermid in ("+sPaymentTermId_sale+") ";
		sPostgreQuery_OMS = sPostgreQuery_OMS + "or paymentTerms = " + sRedeemPaymentTerm +" ) ";

		String sTotalCountQuery = sPostgreQuery_OMS.replace("__columnNames__", "count(1) as TotalRowCount");
		Statement stmt =null;
		ResultSet rs=null;

		int iTotalCount=0;
		try 
		{
			stmt = APS_OMS.postgre_OMS.connection.createStatement();

			rs= stmt.executeQuery(sTotalCountQuery);
			rs.next();
			iTotalCount = rs.getInt("TotalRowCount");
		} 
		catch (SQLException e) {
			String sError = "Not able to execute OMS query : "+sTotalCountQuery;
			LoggerUtil.setlog(LogLevel.ERROR,sError );
			e.printStackTrace();
			olr.errorList.add(sError);
			olr.Status = "FAILED";
			olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
			Assert.assertTrue(false,sError);
		}

		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Total records in OMS to verify -:"+iTotalCount);
		olr.iTotalCount = iTotalCount;
		
		if(iTotalCount==0)
		{
			LoggerUtil.setlog(LogLevel.ERROR, "No more records to verify");
			olr.errorList.add("No more records to verify");
			//olr.Status = "FAILED";
			//olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);

		}
		else
		{
			int totalCounter = (iTotalCount/ilimit_OMSresult) +1;
			for(int offsetCounter=0;offsetCounter<totalCounter;offsetCounter++)
			{
				String sOffset = Integer.toString(ilimit_OMSresult*offsetCounter);
				String sQuery = sPostgreQuery_OMS.replace("__columnNames__", "cda, dealId, merchantId, merchantName,offerid, orderid, orderlineid, unitprice, paymentTerms,paymenttermid, voucherid, unitPrice,createdAt");
				sQuery = sQuery +  "order by orderlineid OFFSET "+ sOffset +" LIMIT "+ Integer.toString(ilimit_OMSresult);

				try 
				{
					stmt = APS_OMS.postgre_OMS.connection.createStatement();
					rs = stmt.executeQuery(sQuery);
				} 
				catch (SQLException e)
				{
					String sError = "Not able to execute OMS query : "+sQuery;
					LoggerUtil.setlog(LogLevel.ERROR,sError );
					e.printStackTrace();
					olr.errorList.add(sError);
					olr.Status = "FAILED";
					olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
					Assert.assertTrue(false,sError);
				}
				//compare this result set with APS data
				try{
					APS_OMS.validate_OMS_APS(rs,olr);}
					catch(Exception e)
					{
						e.printStackTrace();
						olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
						long end_millis = System.currentTimeMillis();
						
						
						LoggerUtil.setlog(LogLevel.ONLYLOGS, "Test Finish Time -:"+end_millis);
						LoggerUtil.setlog(LogLevel.ONLYLOGS, "Time Taken -:"+(end_millis-start_millis));
						break;
					}
			}
		}
		
		long end_millis = System.currentTimeMillis();
		
		
		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Test Finish Time -:"+end_millis);
		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Time Taken -:"+(end_millis-start_millis));
		olr.updateCompleteTestResult(APS_OMS.mongo_groupon, APS_OMS.Collection_Report);
		
	}
	@DataProvider(name = "trigger_redeem")
	public Object[][] triggerRedeem() throws Exception {

		String [][] ret = { {"APS_OMS_TriggerRedeem_Mapping"} };
		return ret;
	}





}
