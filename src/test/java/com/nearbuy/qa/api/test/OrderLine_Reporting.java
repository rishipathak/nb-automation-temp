package com.nearbuy.qa.api.test;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.nearbuy.qa.util.LoggerUtil;
import com.nearbuy.qa.util.LoggerUtil.LogLevel;
import com.nearbuy.qa.util.MongoQuery;
import com.nearbuy.qa.util.MongoUtil;

public class OrderLine_Reporting 
{
	// For Error Reporting
	public String currentOrderLineId;
	public String runID ;
	public long iTotalCount;
	public String Status;
	public ArrayList<String> errorList; 	

	public OrderLine_Reporting(String RunID)
	{
		this.runID = RunID;
		this.errorList = new ArrayList<String>();
		
		this.iTotalCount=0;
		this.Status="FAILED";
		this.currentOrderLineId = "";
	}


	public boolean updateCompleteTestResult(MongoUtil mongo_db,String CollectionName)
	{

		BasicDBObject document = new BasicDBObject();
		document.put("RunId",runID);
		document.put("OrderLineID", currentOrderLineId);
		document.put("OMSTotalRecords",iTotalCount);
		document.put("Status", Status);
		document.put("Errors", errorList);


		try 
		{
		MongoQuery.insertRecod(mongo_db.db, CollectionName,document);
		} 
		catch (Exception e)
		{

			LoggerUtil.setlog(LogLevel.ERROR, "Not able to insert status record in db");
			e.printStackTrace();
		}
		return true;
	}


}
