package com.nearbuy.qa.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 
 * @author puneet
 *
 */
public class ReportUtil {

	public static File extractResultTable(String environment) {

		File file= null;
		try {
			file =new File(System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("TempFile"));
			if(!file.exists())
				file.createNewFile();

			String testngReport	=	System.getProperty("user.dir")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("TestReport")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("SuiteName")+System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("SuiteName")+".html";
			File input = new File(testngReport);
			
			for (int i = 0; i < 6 ; i++) {
				if (!input.exists()){
					Thread.sleep(10000);
				}
				else
					break;
			}
			
			Document doc = Jsoup.parse(input, "UTF-8");
			Element table = doc.select("table").first();
//			Element title = doc.select("h2").first();

			Element row = table.select("tr").first();
			String tableData = row.select("td").last().text();
			String[] totalTest = tableData.split("/");
			int totalTestRun = Integer.parseInt(totalTest[0]) + Integer.parseInt(totalTest[1]) + Integer.parseInt(totalTest[2]);
			
			for (int i = 0; i < 2; i++) {
				Element tableRow = doc.select("table tr:eq(3)").first();
			    tableRow.remove();
			}
			
			table.prepend("<tr><td>Total Tests Run</td><td>"+totalTestRun+"</td></tr>");	
			table.prepend("<tr><td>Environment</td><td>"+environment+"</td></tr>");

//			String html = title.outerHtml() + table.outerHtml();
			String html = table.outerHtml();
			FileWriter writer = new FileWriter(file, true);
			BufferedWriter buf = new BufferedWriter(writer);
			buf.write(html);
			buf.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		return file;
	}

}
