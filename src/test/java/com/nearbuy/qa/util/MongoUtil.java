package com.nearbuy.qa.util;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * 
 * @author puneet
 *
 */
public class MongoUtil {

	MongoClient mongoClient=null;
	public DB db=null;
	
	/**
	 * 
	 * @param mongoDb
	 * @param localPort
	 * @throws Exception
	 */
	public void createMongoConnection(String mongoDb,String localPort) {

		try {
			mongoClient = new MongoClient(PropertiesUtil.getEnvConfigProperty("LocalHost"), Integer.parseInt(localPort));
			this.db = mongoClient.getDB(mongoDb);
			
			System.out.println("db names -");
			System.out.println(mongoClient.getDatabaseNames());
			System.out.println("collectionNames -");
			System.out.println(this.db.getCollectionNames().toString());
			
			System.out.println("MongoDB connection created successfully !!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeConnectionMongo() {
		this.mongoClient.close();
		System.out.println("MongoDB connection closed successfully !!!");
	}
	
	
}
