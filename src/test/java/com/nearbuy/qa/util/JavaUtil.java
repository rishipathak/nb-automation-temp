package com.nearbuy.qa.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author puneet
 *
 */
public class JavaUtil {

	/**
	 * This functions is to Return a Random String Name
	 * @param length :- length of String
	 * @return
	 */

	public static String randomString(int length) {

		char[] characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
		//char[] intArray = "1234567890".toCharArray();
		Random random = new SecureRandom();
		char[] result = new char[length];
		//		char[] result2 = new char[length];
		for (int i = 0; i < result.length; i++) {
			int randomCharIndex = random.nextInt(characterSet.length);
			result[i] = characterSet[randomCharIndex];
		}

		//		for (int i = 0; i < result2.length; i++) {
		//			int randomIntIndex = random.nextInt(intArray.length);
		//			result2[i] = intArray[randomIntIndex];
		//		}

		//		return new String(result).concat(new String(result2));
		return new String(result);
	}

	/**
	 * This functions is to Return a Number
	 * @param length :- length of Number
	 * @return
	 */
	public static String randomNumber(int length) {

		char[] intArray = "1234567890".toCharArray();
		Random random = new SecureRandom();
		char[] result = new char[length];

		for (int i = 0; i < result.length; i++) {
			int randomIntIndex = random.nextInt(intArray.length);
			result[i] = intArray[randomIntIndex];
		}

		return new String(result);
	}

	/**
	 * To start/stop the XVFB server  
	 * @param filename
	 * @return
	 */

	public static String xvfbServerStartStop(String scriptPath){

		Runtime r = Runtime.getRuntime();
		Process p ;
		StringBuffer output = new StringBuffer();

		try {
			p = r.exec(scriptPath);
			BufferedReader reader = 
					new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}
			System.out.println("Value ::"+output.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	
	/**
	 * This function is used to replace values from the Map in the string.
	 * @param replacePreRequisite : Map who's values to be replaced in the string.
	 * @param queryParamJson : Text in which the replacement needs to be done.
	 * @return
	 */
	public static String replacePreReuisite(Map<String, String> replacePreRequisite,String queryParamJson) {

		if (queryParamJson.equals("null") || queryParamJson == null)
			return null;
		String patternString = "__(" + StringUtils.join(replacePreRequisite.keySet(), "|") + ")__";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(queryParamJson);
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
			//			System.out.println("Value inside Matcher ::" +replacePreRequisite.get(matcher.group(1)));
			matcher.appendReplacement(sb, replacePreRequisite.get(matcher.group(1)));
		}
		matcher.appendTail(sb);

		return sb.toString();

	}
}
