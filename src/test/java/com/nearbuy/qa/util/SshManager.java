package com.nearbuy.qa.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.LocalPortForwarder;

/**
 * 
 * @author puneet
 *
 */
public class SshManager {

	private static Connection connection = null;
	private static List<LocalPortForwarder> lpf = null;
	
	
	/**
	 * This function is used to create a tunnel from bastion to database server
	 * @param mongoIp : Ip of the database server.
	 * @param mongoPort : Port of the mongo/sql/postgre server to be read from the environment file.
	 * @param localPort : Local port for the bastion server from which the tunneling will be done.To be read from the environment file  
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	public static void createSshConnection(String mongoIp,String mongoPort,String localPort,String env) throws Exception {
		try {
			if(env.equals("prod")){
			createSession(PropertiesUtil.getEnvConfigProperty("BastionIP_prod"), PropertiesUtil.getEnvConfigProperty("SshUser"), mongoIp, Integer.parseInt(mongoPort),localPort,env);
			}else if (env.equals("qa")){
			createSession(PropertiesUtil.getEnvConfigProperty("BastionIP_qa"), PropertiesUtil.getEnvConfigProperty("SshUser"), mongoIp, Integer.parseInt(mongoPort),localPort,env);
			}
			} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static void createSession(String sship,String user,String dbip,int forwardPort,String localPort,String env) throws Exception
	{
		SshManager manager = new SshManager();
		lpf = new ArrayList<LocalPortForwarder>();
		manager.ssh(sship,user,env);
		manager.forwardLocalPort(Integer.parseInt(localPort),dbip,forwardPort);
	}

	private void forwardLocalPort(Integer localport, String remotehost,Integer remoteport) throws InterruptedException {
		try {
			LocalPortForwarder temp = connection.createLocalPortForwarder(localport, remotehost, remoteport);
			lpf.add(temp);
		} catch (IOException e) {
			//e.printStackTrace();

		}
		Thread.sleep(5000);
	}

	private void ssh(String sship,String sshusername,String env) throws Exception {
		try {
			connection = new Connection(sship, 22);
			connection.connect();
			File key = null;
			String path="";
			if(env.equals("prod")){
			path = System.getProperty("user.dir")+ System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("PrivateKey_prod");
			}else if (env.equals("qa")){
			path = System.getProperty("user.dir")+ System.getProperty("file.separator")+PropertiesUtil.getConstantProperty("PrivateKey_qa");
			}
			File f = new File(path);
			if (f.exists()){
				key = new File(path);
			}
//			else{
//				key = new File(System.getProperty("user.dir")+ FileUtil.separator+FileUtil.getConstantValue("ProjectName")+FileUtil.separator+FileUtil.getConstantValue("PrivateKey"));
//			}
			boolean isAuthenticated = connection.authenticateWithPublicKey(
					sshusername, key, null);
			if (isAuthenticated == false) {
				throw new IOException("Authentication failed.");
			}
	        System.out.println("SSH connection created successfully !!!");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void closeConnection() {
		if (connection != null)
			connection.close();
		System.out.println("SSH connection closed !!!");

	}
}
