package com.nearbuy.qa.util;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.lang3.StringUtils
;

/**
 * 
 * @author puneet
 *
 */
public class PostgreUtil {

	public Connection connection;

	/**
	 * This function is used to create a Postgre connection
	 * @param localPort : localport of the bastion to be read from the environment file. 
	 * @param username : username to conncet to database.
	 * @param password : password to connect to database.
	 */
	public void postgreConnection(String localPort,String username,String password,String database) {

		try {
			Class.forName("org.postgresql.Driver");
			if(StringUtils.isNotEmpty(database))
				connection = DriverManager.getConnection("jdbc:postgresql://"+PropertiesUtil.getEnvConfigProperty("LocalHost")+":"+localPort+"/"+database, username, password);
			else
				connection = DriverManager.getConnection("jdbc:postgresql://"+PropertiesUtil.getEnvConfigProperty("LocalHost")+":"+localPort, username, password);
			System.out.println("Postgre DB connection created successfully !!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closePostgreConnection() {
		try {
			if(connection != null){
				connection.close();
				System.out.println("PSQL DB connection closed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}