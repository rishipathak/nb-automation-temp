package com.nearbuy.qa.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

public class MongoQuery {

	static DBCollection collection;

	public static ArrayList<Object> findWithQuery(DB db,String collectionName, BasicDBObject query,int limit,String Key){

		ArrayList<Object> results = new ArrayList<Object>();
		System.out.println(db.getCollectionNames().toString());
		collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find(query).limit(limit);
		

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				results.add(obj.get(Key));
			}

		} finally {
			cursor.close();
		}

		return results;

	} 

	public static ArrayList<Object> findWithQuery(DB db,String collectionName, BasicDBObject query,BasicDBObject field,BasicDBObject sortField) throws Exception{


		ArrayList<Object> results = new ArrayList<Object>();
		collection = db.getCollection(collectionName);

		DBCursor cursor = null;
		if (field != null){
			if (sortField != null)
				cursor = collection.find(query,field).sort(sortField);
			else
				cursor = collection.find(query,field);
		}
		else 
			cursor = collection.find(query);
		try {
//			int i = 1;
			while (cursor.hasNext()) {
//				System.out.println("count -"+i);
				DBObject obj = cursor.next();
				//results.add(obj);
				if (field != null){
					for (String keys : obj.keySet()) {
						//results.add(obj.get(keys));
						System.out.println("Keys -"+keys);
					}
					results.add(obj);
				}else
					results.add(obj);
//				i++;
			}

		} 
		finally {
			cursor.close();
		}

		return results;

	} 

	public static ArrayList<Object> findWithQuery(DB db,String collectionName, BasicDBObject query,BasicDBObject field,BasicDBObject sortField, int limit) throws Exception{

		ArrayList<Object> results = new ArrayList<Object>();
		collection = db.getCollection(collectionName);

		DBCursor cursor = null;
		if (field != null){
			if (sortField != null)
				cursor = collection.find(query,field).sort(sortField).limit(limit);
			else
				cursor = collection.find(query,field).limit(limit);
		}
		else {
			if (sortField != null)
				cursor = collection.find(query).sort(sortField).limit(limit);
			else
				cursor = collection.find(query).limit(limit);
		}
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				//results.add(obj);
				if (field != null){
					for (String keys : obj.keySet()) {
						//results.add(obj.get(keys));
						System.out.println("Keys -"+keys);
					}
					results.add(obj);
				}else
					results.add(obj);
			}

		} 
		finally {
			cursor.close();
		}

		return results;

	} 
	
	public static ArrayList<Object> findWithQuerySkipingFirntNRecords(DB db,String collectionName, BasicDBObject query,BasicDBObject field,BasicDBObject sortField, int limit, int skpipcount) throws Exception{

		ArrayList<Object> results = new ArrayList<Object>();
		collection = db.getCollection(collectionName);

		DBCursor cursor = null;
		if (field != null){
			if (sortField != null)
				cursor = collection.find(query,field).skip(skpipcount-1).sort(sortField).limit(limit);
			else
				cursor = collection.find(query,field).skip(skpipcount-1).limit(limit);
		}
		else {
			if (sortField != null)
				cursor = collection.find(query).sort(sortField).skip(skpipcount-1).limit(limit);
			else
				cursor = collection.find(query).skip(skpipcount-1).limit(limit);
		}
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				//results.add(obj);
				if (field != null){
					for (String keys : obj.keySet()) {
						//results.add(obj.get(keys));
						System.out.println("Keys -"+keys);
					}
					results.add(obj);
				}else
					results.add(obj);
			}

		} 
		finally {
			cursor.close();
		}

		return results;

	} 
	
	public static int countingQuery(DB db,String collectionName, BasicDBObject query) throws Exception{

		collection = db.getCollection(collectionName);
		int count = collection.find(query).count();

		return count;
	} 

	public static Object insertRecod(DB db,String collectionName, BasicDBObject document) throws Exception{

		collection = db.getCollection(collectionName);
		WriteResult result = collection.insert(document);

		
		return result.getField("_id");
	} 
	
	public static int removeManyWithQuery(DB db,String collectionName,BasicDBObject query){

		if(query == null){
			System.out.println("Removing all rows not supported");
			return 0;
		}
//		System.out.println(query.toString());
		collection = db.getCollection(collectionName);
		WriteResult c1 = collection.remove(query);

		return c1.getN();

	}
	
	public static int removeSpecificField(DB db,String collectionName,BasicDBObject query,BasicDBObject update){
		
		if(query == null){
			System.out.println("Removing all rows not supported");
			return 0;
		}
		collection = db.getCollection(collectionName);
		WriteResult updateField = collection.update(query, update);
		return updateField.getN();
		
	}

}
