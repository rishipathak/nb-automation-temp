package com.nearbuy.qa.util;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import com.mongodb.BasicDBObject;
import com.nearbuy.qa.api.test.OrderLine_Reporting;
import com.nearbuy.qa.util.LoggerUtil.LogLevel;

public class APS_OMS_MappingUtil {

	public  HashMap<String, String> OMS_APS_Map;
	public MongoUtil mongo_lookup;
	public MongoUtil mongo_APS;
	public PostgreUtil postgre_OMS ;
	public MongoUtil mongo_groupon;
	public String Collection_lookup;
	public String Collection_APS;
	public String Collection_Report;//OMS_APS_MappingReport

	public APS_OMS_MappingUtil() 
	{

		LoggerUtil.setlog(LogLevel.ONLYLOGS, "*********** Initializing class variables for APSOMSmapping *************");

		OMS_APS_Map = new HashMap<String,String>();
		mongo_lookup = new MongoUtil();
		mongo_APS = new MongoUtil();
		postgre_OMS = new PostgreUtil();
		mongo_groupon = new MongoUtil();
		
		Collection_lookup = PropertiesUtil.getEnvConfigProperty("collection_lookup");
		Collection_APS = PropertiesUtil.getEnvConfigProperty("collection_APS");
		Collection_Report = PropertiesUtil.getEnvConfigProperty("collection_APSOMSReport");
		initilizeMap();


	}

	public void createDBConnections() throws Exception
	{

		// creating postrgess connection with OMS
		SshManager.createSshConnection(PropertiesUtil.getEnvConfigProperty("OMSIp"), PropertiesUtil.getEnvConfigProperty("OMSPort"),PropertiesUtil.getEnvConfigProperty("LocalPortPostgreOMS"),"prod");
		postgre_OMS.postgreConnection(PropertiesUtil.getEnvConfigProperty("LocalPortPostgreOMS"),PropertiesUtil.getEnvConfigProperty("PostGreUsernameOMS"),PropertiesUtil.getEnvConfigProperty("PostGrePasswordOMS"),PropertiesUtil.getEnvConfigProperty("DatabaseName"));

		//APS-DB

		SshManager.createSshConnection(PropertiesUtil.getEnvConfigProperty("MongoIp_APS").trim(),PropertiesUtil.getEnvConfigProperty("MongoPort_APS").trim(),PropertiesUtil.getEnvConfigProperty("LocalPortAPS"),"prod");
		mongo_APS.createMongoConnection(PropertiesUtil.getEnvConfigProperty("MongoDB_APS"),PropertiesUtil.getEnvConfigProperty("LocalPortAPS"));

		
		//lookup-DB

		SshManager.createSshConnection(PropertiesUtil.getEnvConfigProperty("MongoIp_lookup").trim(),PropertiesUtil.getEnvConfigProperty("MongoPort_lookup").trim(),PropertiesUtil.getEnvConfigProperty("LocalPortLookup"),"prod");
		mongo_lookup.createMongoConnection(PropertiesUtil.getEnvConfigProperty("MongoDB_lookup"),PropertiesUtil.getEnvConfigProperty("LocalPortLookup"));

		//Groupon-DB (for Reporting)

		SshManager.createSshConnection(PropertiesUtil.getEnvConfigProperty("MongoIp_groupon").trim(),PropertiesUtil.getEnvConfigProperty("MongoPort_groupon").trim(),PropertiesUtil.getEnvConfigProperty("LocalPortGroupon"),"qa");
		mongo_groupon.createMongoConnection(PropertiesUtil.getEnvConfigProperty("MongoDB_groupon"),PropertiesUtil.getEnvConfigProperty("LocalPortGroupon"));

		
				
		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Mongo Connection established successfully");
	}

	public  void initilizeMap()
	{

		//key APS field and value is corresponding OMS field
		//OMS_APS_Map.put("merchantPartnerNumber", "partnernumber");
		OMS_APS_Map.put("orderId", "orderid");
		OMS_APS_Map.put("orderLineId","orderlineid");
		//OMS_APS_Map.put("voucherId", "voucherid");
		OMS_APS_Map.put("dealId", "dealid");
		OMS_APS_Map.put("offerId", "offerid");
		OMS_APS_Map.put("merchantId", "merchantid");
	//	OMS_APS_Map.put("payableAmount", "dummy");//this field in oms is calucated based upon "unitprice and margin%"
	}

	public void closeDbConnections()
	{
		mongo_lookup.closeConnectionMongo();
		postgre_OMS.closePostgreConnection();
		mongo_APS.closeConnectionMongo();
		mongo_groupon.closeConnectionMongo();
		SshManager.closeConnection();

		LoggerUtil.setlog(LogLevel.ONLYLOGS, "Database Connection Closed");
	}

	public String fetchNodeValue(String expectedNode, JsonNode rootNode) throws Exception
	{
		String nodeValue = "";
		if (expectedNode.split("\\.").length==1)
		{
			rootNode = rootNode.get(expectedNode);

			if(rootNode.isDouble()|| rootNode.isNumber()||rootNode.isInt())
			{
				nodeValue = rootNode.toString().replaceAll("\\.0", "");
			}
			else
			{
				nodeValue =  rootNode.toString();
			}

		}
		else
		{
			String [] test  = expectedNode.split("\\.",2);
			String  Node = test[0];

			rootNode = rootNode.get(Node);

			if(rootNode.isContainerNode())
			{
				Iterator<Map.Entry<String, JsonNode>> ChildNodes = rootNode.getFields();
				Iterator<JsonNode> NodeArrayIterator = rootNode.getElements();
				if (ChildNodes.hasNext()) 
				{ //if expectedNode is an Object, and fields exist i.e.general case
					nodeValue =  fetchNodeValue(test[1],rootNode);
				}
				else if(NodeArrayIterator.hasNext()) 
				{ //if expectedNode is a Json Array, and child elements exist 

					while(NodeArrayIterator.hasNext())
					{
						nodeValue = nodeValue + fetchNodeValue(test[1],NodeArrayIterator.next()) +",";
					}

				}
			}

			if (rootNode.isValueNode())
			{
				if(rootNode.isDouble()|| rootNode.isNumber()||rootNode.isInt())
				{
					nodeValue = rootNode.toString().replaceAll("\\.0", "");
				}
				else
				{
					nodeValue =  rootNode.toString();
				}

			}	
		}
		return nodeValue;

	}

	public String getPaymetid(String strigger,OrderLine_Reporting olr) 
	{
		BasicDBObject lookup_query = new BasicDBObject();
		lookup_query.put("trigger", strigger);
		lookup_query.put("payment", "auto");


		ArrayList<Object> lookQueryResult=null;;
		try {
			lookQueryResult = MongoQuery.findWithQuery(mongo_lookup.db, Collection_lookup, lookup_query, null,null);
		} 
		catch (Exception e) 
		{
			String sError = "Failed to execute mongo query for paymenttermid in lookup hence terminate";
			LoggerUtil.setlog(LogLevel.ERROR,sError );
			e.printStackTrace();
			olr.errorList.add(sError);
			olr.Status = "FAILED";
			olr.updateCompleteTestResult(mongo_groupon, Collection_Report);
			Assert.assertTrue(false,sError);

		}

		ObjectMapper mapper = new ObjectMapper();
		String salePaymentId = "";
		for(int lookupcount = 0; lookupcount<lookQueryResult.size();lookupcount++)
		{
			JsonNode lookupNode=null;;
			try {
				lookupNode = mapper.readTree(lookQueryResult.get(lookupcount).toString());
			} 
			catch (JsonProcessingException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e) 
			{	
				e.printStackTrace();
			}
			try {
				salePaymentId =salePaymentId + fetchNodeValue("id",lookupNode).replaceAll("\\x5B", "").replaceAll("\\x5D", "").replaceAll("\\x22", "").replaceAll(",,",",") +",";
			} 
			catch (Exception e)
			{
				String sError = "Failed to Find id node in lookup jason tree or paymenttermid hence terminate";
				LoggerUtil.setlog(LogLevel.FAIL,sError );
				e.printStackTrace();
				olr.errorList.add(sError);
				olr.Status = "FAILED";
				olr.updateCompleteTestResult(mongo_groupon, Collection_Report);
				Assert.assertTrue(false,sError);
			}

		}
		salePaymentId = salePaymentId.substring(0, salePaymentId.length()-1);

		return salePaymentId;
	}

	public String convertToEpoch(String sDate,OrderLine_Reporting olr) 
	{
		String sEpochTime = "";

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date=null;;
		try 
		{
			date = format.parse(sDate);
		} 
		catch (ParseException e)
		{
			String sError = "Not able to parse the give date format";
			LoggerUtil.setlog(LogLevel.ERROR,sError );
			e.printStackTrace();
			olr.errorList.add(sError);
			olr.Status = "FAILED";
			olr.updateCompleteTestResult(mongo_groupon, Collection_Report);
			
			Assert.assertTrue(false,sError);
		}
		long epoch = date.getTime();
		sEpochTime = Long.toString(epoch); // 1055545912454

		return sEpochTime;		
	}

	public boolean compareRecords(String sAPSRecord,ResultSet OMSRecord,OrderLine_Reporting olr) throws Exception
	{
		boolean bValidation = true;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode APSNodeTree = mapper.readTree(sAPSRecord);

		Set<String> APSkeys = OMS_APS_Map.keySet();
		Iterator<String> it_apsKey = APSkeys.iterator();

		while(it_apsKey.hasNext())
		{
			String APSField = it_apsKey.next();
			String OMSField = OMS_APS_Map.get(APSField);

			String APSValue="";
			String OMSValue = "";


			if(APSField.equals("payableAmount"))
			{
				APSValue = fetchNodeValue(APSField, APSNodeTree);
				APSValue = Double.toString(Double.parseDouble(APSValue)*100); //converting in paise

				Double dUnitPrice = Double.parseDouble(OMSRecord.getString("unitprice"));
				Double dMarginperc = Double.parseDouble(OMSRecord.getString("marginpercentage"));

				Double dOMSvlaue =  dUnitPrice*((100-dMarginperc)/100);
				OMSValue = Double.toString(dOMSvlaue);
			}
			else
			{
				APSValue = fetchNodeValue(APSField, APSNodeTree);
				OMSValue = OMSRecord.getString(OMSField);
			}

			APSValue = APSValue.replaceAll("\\x5B", "").replaceAll("\\x5D", "").replaceAll("\\x22", "").replaceAll(",,",",");
			//OMSValue = OMSValue.replaceAll("\\x5B", "").replaceAll("\\x5D", "").replaceAll("\\x22", "").replaceAll(",,",",");


			if(APSValue.equals(OMSValue))
			{
				String sPassComment = "PASS: Field "+OMSField+" Match in APS and OMS for Orderline ID : " +OMSRecord.getString("orderlineid");
				LoggerUtil.setlog(LogLevel.PASS,sPassComment);
				olr.errorList.add(sPassComment);
				
			}
			else
			{
				bValidation = false;
				String sError = "FAIL: Field "+OMSField+" NOT Match in APS and OMS for Orderline ID : " +OMSRecord.getString("orderlineid");
				LoggerUtil.setlog(LogLevel.FAIL,sError);
				olr.errorList.add(sError);
				olr.Status = "FAILED";
			}

		}



		return bValidation;
	}

	public void validate_OMS_APS(ResultSet OMSrecords_Postgre,OrderLine_Reporting olr) throws Exception 
	{
		//boolean bValidation=false;
		String sOrderlineId="";
		
		//xsolr_Validate.
		
		try {
			while(OMSrecords_Postgre.next())
			{
				OrderLine_Reporting olr_Validate = new OrderLine_Reporting(olr.runID);
				olr_Validate.Status = "PASSED";
				olr_Validate.errorList.clear();
				olr_Validate.iTotalCount=1;
				sOrderlineId = OMSrecords_Postgre.getString("orderlineid");
				String sApsRecord_mongo = "";
				olr_Validate.currentOrderLineId = sOrderlineId;
				BasicDBObject APS_query = new BasicDBObject();
				APS_query.put("orderLineId", sOrderlineId);
				ArrayList<Object> APSQueryResult = new ArrayList<>();
				try{
					 APSQueryResult = MongoQuery.findWithQuery(mongo_APS.db, Collection_APS, APS_query, null,null);
				}catch(Exception e){
					String sError = "FAIL: Problem occured while fetching record from APS for orderlinr ID :"+sOrderlineId + " ---- " +e.getStackTrace().toString();
					LoggerUtil.setlog(LogLevel.FAIL, sError);
					olr_Validate.errorList.add(sError);
					olr_Validate.Status = "FAILED";
					e.printStackTrace();
					olr_Validate.updateCompleteTestResult(mongo_groupon, Collection_Report);
					throw e;
				}
				try{
					if(APSQueryResult.size()>0)
					{
						sApsRecord_mongo =  APSQueryResult.get(0).toString();
						//comparerecords

						if(compareRecords(sApsRecord_mongo,OMSrecords_Postgre,olr_Validate))
						{
							String sPassComment = "PASS: All Field Matches in APS and OMS for Orderline ID : " +sOrderlineId;
							LoggerUtil.setlog(LogLevel.PASS,sPassComment);
							olr_Validate.errorList.add(sPassComment);
							//olr.Status = "PASSED";
						}
						else
						{
							String sError = "FAIL: Some Fields Did not Matches in APS and OMS for Orderline ID : " +sOrderlineId;
							LoggerUtil.setlog(LogLevel.FAIL,sError);
							olr_Validate.errorList.add(sError);
							olr_Validate.Status = "FAILED";
						}


					}
					else
					{
						String sError = "FAIL: record not found in APS for order line id : " +sOrderlineId;
						LoggerUtil.setlog(LogLevel.FAIL,sError);
						olr_Validate.errorList.add(sError);
						olr_Validate.Status = "FAILED";

					}
				}
				catch (Exception e) 
				{
					String sError = "FAIL: Some mapped fields are not present in OMS or APS DB for orderlinr ID :"+sOrderlineId;
					LoggerUtil.setlog(LogLevel.FAIL, sError);
					olr_Validate.errorList.add(sError);
					olr_Validate.Status = "FAILED";
					e.printStackTrace();
					olr_Validate.updateCompleteTestResult(mongo_groupon, Collection_Report);
					continue;
				}

				olr_Validate.updateCompleteTestResult(mongo_groupon, Collection_Report);
			}
		} 
		catch (SQLException e) 
		{
			String sError = "Lost connection or result is currupted for OMS DB please terminate and start the test again";
			LoggerUtil.setlog(LogLevel.ERROR,sError );
			olr.errorList.add(sError);
			olr.Status = "FAILED";
			e.printStackTrace();
			olr.updateCompleteTestResult(mongo_groupon, Collection_Report);
			
			Assert.assertTrue(false,sError);
		} 

		
	}



}
